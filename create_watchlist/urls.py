from django.urls import path
from.views import signup, login_view, index, logout_view, search_movie, create_watchlist, watchlist, remove_watchlist

urlpatterns = [

    path('', signup, name='signup'),
    path('login/', login_view, name='login'),
    path('index/', index, name='index'),
    path('logout/', logout_view, name='logout'),
    path('search/', search_movie, name='search'),
    path('create-watchlist/', create_watchlist, name='create_watchlist'),
    path('remove-watchlist/', remove_watchlist, name='remove_watchlist'),
    path('watchlist/', watchlist, name='watchlist')

]