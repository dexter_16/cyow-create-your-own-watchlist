from django.apps import AppConfig


class CreateWatchlistConfig(AppConfig):
    name = 'create_watchlist'
