from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class WatchList(models.Model):
	wl_id 	 = models.AutoField(primary_key=True, db_column= 'wl_id')
	user	 = models.ForeignKey(User, on_delete = models.CASCADE)
	movie_id = models.IntegerField()
	
	def __str__(self):
		return str(self.user)+'-'+str(self.movie_id)

	class Meta:
		unique_together = (('user', 'movie_id'),)