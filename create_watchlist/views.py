from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.db import IntegrityError
from .models import WatchList
import http.client
import json
from urllib.parse import quote

def signup(request):

    if request.method == 'POST':
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)

            return redirect('/index/')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})


def login_view(request):
	
	if request.method == 'POST':
		form = AuthenticationForm(data = request.POST)

		if form.is_valid():
			
			username = form.cleaned_data.get('username')
			raw_password = form.cleaned_data.get('password')
			user = authenticate(username=username, password=raw_password)
			login(request, user)

			return redirect('/index/')
	else:
		form = AuthenticationForm()

	return render(request, 'login.html', {'form': form})


def logout_view(request):

    logout(request)

    return redirect('/')


def index(request):
	
	response ={}
	user_id = request.session['_auth_user_id']
	try:
		response['user_obj'] = User.objects.get(id = user_id)
	except User.DoesNotExist:
		response['user_obj'] = ''

	watchlist_ids = WatchList.objects.values_list('movie_id', flat = True).filter(user_id = user_id)

	conn = http.client.HTTPSConnection("api.themoviedb.org", timeout=1)
	payload = "{}"
	conn.request("GET", "/3/search/movie?query=iron%20man&language=en-US&api_key=9311153d8eeaabfc90d5913ca73107a6", payload)

	try:
		json_data = json.loads(conn.getresponse().read().decode('utf-8'))
		for data in json_data['results']:
			if data['id'] in watchlist_ids:
				data['is_already_added'] = True
			else:
				data['is_already_added'] = False
	except (AttributeError, KeyError, IndexError):
		json_data = ''
		
	
	
	response['data'] = json_data

	return render(request, 'index.html', response)


def search_movie(request):
	
	response = {}
	query = quote(request.GET.get('search', None))
	conn = http.client.HTTPSConnection("api.themoviedb.org")
	payload = "{}"
	conn.request("GET", "/3/search/movie?query="+query+"&language=en-US&api_key=9311153d8eeaabfc90d5913ca73107a6", payload)
	watchlist_ids = WatchList.objects.values_list('movie_id', flat = True).filter(user_id = request.session['_auth_user_id'])

	try:

		json_data = json.loads(conn.getresponse().read().decode('utf-8'))

		for data in json_data['results']:
			if data['id'] in watchlist_ids:
				data['is_already_added'] = True
			else:
				data['is_already_added'] = False

	except (AttributeError, KeyError, IndexError):

		json_data = ''

	response['search_result'] = render_to_string('search.html', {'data': json_data})
	
	return JsonResponse(response)


def create_watchlist(request):
	
	response = {}
	movie_id = request.GET.get('movie_id')
	user_id = request.session['_auth_user_id']

	try:
		wl_obj = WatchList.objects.create(user_id = user_id, movie_id = movie_id)
		response['result'] = 'success'
	except IntegrityError:
		response['result'] = 'error'

	return JsonResponse(response)


def remove_watchlist(request):

	response = {}
	movie_id = request.GET.get('movie_id')
	user_id = request.session['_auth_user_id']

	try:
		wl_obj = WatchList.objects.get(user_id = user_id, movie_id = movie_id)
		wl_obj.delete()
		response['result'] = 'success'

	except WatchList.DoesNotExist:
		response['result'] = 'error'

	return JsonResponse(response)


def watchlist(request):
	
	response = {}
	watchlist = []
	wl_obj = WatchList.objects.filter(user_id = request.session['_auth_user_id'])
	conn = http.client.HTTPSConnection("api.themoviedb.org")
	payload = "{}"

	for obj in wl_obj:
		watch_list_dict = {}
		movie_id = quote(str(obj.movie_id))
		conn.request("GET", "/3/movie/"+movie_id+"?api_key=9311153d8eeaabfc90d5913ca73107a6", payload)

		try:
			data = json.loads(conn.getresponse().read().decode('utf-8'))
			watch_list_dict['overview'] = data['overview']
			watch_list_dict['poster_path'] = data['poster_path']
			watch_list_dict['title'] = data['title']
			watch_list_dict['id'] = data['id']

		except (AttributeError, KeyError, IndexError):
			data = ''

		watchlist.append(watch_list_dict)

	response['data'] = watchlist

	return render(request, 'watchlist.html', response)