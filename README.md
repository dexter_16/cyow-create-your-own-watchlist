# CYOW-create your own watchlist

Search movies and create your own watchlist


Setup instruction

1. Clone repo:
	git clone https://gitlab.com/dexter_16/cyow-create-your-own-watchlist.git

2. Create virtual environment:
	virtualenv -p python3 'Env name'

3. Activate your virtual environment:
	source 'Env name'/bin/activate

4. Install Dependencies:
	pip install -r requirements.txt

5. There you go ENJOY !!!
